package main

import (
	"log"

	"github.com/beckler/postmaster/server"
)

func main() {
	s := server.Bootstrap()
	log.Fatal(s.ListenAndServe())
}
