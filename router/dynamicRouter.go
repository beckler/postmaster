package router

import "fmt"

//RegisterDynamicRoutes registers the dynamic routes with a dynamic handler
func RegisterDynamicRoutes() {
	fmt.Println("Registering dynamic routes")
}
