package router

import (
	"fmt"
	"html"
	"net/http"
)

//RegisterStaticRoutes registers the static routes
func RegisterStaticRoutes() {
	fmt.Println("Registering static routes.")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})
}
