package server

import (
	"net/http"
	"time"

	"github.com/beckler/postmaster/router"
)

//Bootstrap initializes the server
func Bootstrap() *http.Server {
	return registerRoutes(createInstance())
}

func registerRoutes(server *http.Server) *http.Server {
	router.RegisterStaticRoutes()
	router.RegisterDynamicRoutes()
	return server
}

func createInstance() *http.Server {
	return &http.Server{
		Addr:           ":8080",
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
}
