FROM golang

ADD . /go/src/github.com/beckler/postmaster

RUN go install github.com/beckler/postmaster

ENTRYPOINT /go/bin/postmaster

EXPOSE 8080
