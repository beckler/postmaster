package adapters

//Messenger interface is for interfacing with different message protocols
type Messenger interface {
	connect()
	send()
	listen()
}
